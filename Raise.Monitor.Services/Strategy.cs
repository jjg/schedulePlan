﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using NLog;
using Oracle.ManagedDataAccess.Client;
using Raise.Monitor.Model;
using Raise.Monitor.Tools;

namespace Raise.Monitor.Services {
    public abstract class Strategy {
        private IDbConnection _connection;

        protected readonly Logger Logger = LogManager.GetCurrentClassLogger(typeof(Strategy));

        protected virtual IDbConnection Connection {
            get {
                var connection = ConfigurationManager.ConnectionStrings["Setting|Monitor"];
                if(connection != null) {
                    switch(connection.ProviderName.ToUpper()) {
                        case "MSSQL":
                            _connection = new SqlConnection(connection.ConnectionString);
                            break;
                        case "MYSQL":
                            _connection = new MySqlConnection(connection.ConnectionString);
                            break;
                        case "ORACLE":
                            _connection = new OracleConnection(connection.ConnectionString);
                            break;
                    }
                }
                return _connection;
            }
        }

        /// <summary>
        /// 查询字典项
        /// </summary>
        /// <param name="name">字典项名称</param>
        /// <returns>KeyValueItem集合</returns>
        public virtual List<KeyValueItem> GetKeyValueItems(string name) {
            return null;
        }

        /// <summary>
        /// 获取定时任务的运行日志
        /// </summary>
        /// <param name="pageIndex">索引页</param>
        /// <param name="keyWords">关键字</param>
        /// <param name="method">请求方法</param>
        /// <param name="serviceName">服务名称</param>
        /// <param name="status">状态</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns>定时任务的运行日志</returns>
        public virtual PageData<MonitorLogRecordConfigView> GetConfigRulesWithLogs(int pageIndex, string keyWords, string method, string serviceName, int? status, DateTime beginTime, DateTime endTime) {
            return null;
        }

        /// <summary>
        /// 获取调度工具的运行日志
        /// </summary>
        /// <param name="pageIndex">索引页</param>
        /// <param name="keyWords">关键字</param>
        /// <param name="callsite"></param>
        /// <param name="logLevel">日志级别</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns>调度工具运行日志</returns>
        public virtual PageData<SystemLog> GetSystemLogs(int pageIndex, string keyWords, string callsite, string logLevel, DateTime beginTime, DateTime endTime) {
            return null;
        }

        /// <summary>
        /// 获取配置
        /// </summary>
        /// <param name="pageIndex">索引页</param>
        /// <param name="keyWords">关键字</param>
        /// <param name="requestType">请求类型</param>
        /// <param name="serviceName">服务名称</param>
        /// <param name="runStatus">运行状态</param>
        /// <param name="status">状态</param>
        /// <returns>当前页的配置</returns>
        public virtual PageData<RuleConfig> GetConfigRules(int pageIndex, string keyWords, string requestType, string serviceName, int? runStatus, int? status) {
            return null;
        }

        /// <summary>
        /// 设置配置的状态
        /// </summary>
        /// <param name="id">配置Id</param>
        /// <param name="status">状态</param>
        public virtual void SetRuleConfigStatus(int id, int status) {
        }

        /// <summary>
        /// 设置配置的运行状态
        /// </summary>
        /// <param name="id">配置Id</param>
        /// <param name="runStatus">运行状态</param>
        public virtual void SetRuleConfigRunStatus(int id, int runStatus) {
        }

        /// <summary>
        /// 新增调度配置
        /// </summary>
        /// <param name="config">配置</param>
        /// <returns>保存结果</returns>
        public virtual MessageInformation InsertRule(RuleConfig config) {
            return null;
        }

        /// <summary>
        /// 修改调度配置
        /// </summary>
        /// <param name="config">配置</param>
        /// <returns>保存结果</returns>
        public virtual MessageInformation SaveChanges(RuleConfig config) {
            return null;
        }

        /// <summary>
        /// 根据ID查询配置
        /// </summary>
        public virtual RuleConfig GetRuleConfigById(int id) {
            return null;
        }

        public virtual Strategy Execute() {
            return null;
        }
    }
}
